'use strict';
const jwt = require('jsonwebtoken');
const CONFIG = require('../config/app_config');

module.exports = (sequelize, DataTypes) => {
    let Model = sequelize.define('User', {
        user_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        first_name: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        last_name: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        email_id: {
            type: DataTypes.STRING,
            defaultValue: null
        },        
        password: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        gender: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
            comment: "1=>male 2=>female"
        },
        age: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        phone_number: {
            type: DataTypes.STRING,
            defaultValue: null
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
            comment: "1=>active 2=>inactive 3=>deleted"
        },
    }, {
        timestamps: true,
        deletedAt: 'deletedAt',
        paranoid: true,
        tableName: 'users',
    });
    sequelize.sync({
        force: false,
        logging: false,
        // alter: true
    });


    Model.associate = function(models) {
        // this.Model = Model.hasOne(models.CancerType, { sourceKey: 'cancer_type_id', foreignKey: 'cancer_type_id', as: 'cancer_type' })
        // this.Model = Model.hasMany(models.Answer, {
        //     sourceKey: 'user_id',
        //     foreignKey: 'user_id',
        //     as: 'answers'
        // })

    };

    Model.prototype.comparePassword = async function(pw) {
        let err;
        let pass;
        if (!this.password) TE('password not set');
        if (pw == this.password) {
            return this;

        } else {
            if (!pass) throw new Error('invalid password');

        }

        return this;
    };

    Model.prototype.getJWT = function() {
        let expiration_time = parseInt(CONFIG.jwt_expiration);
        console.error(this.user_id);
        return 'Bearer ' + jwt.sign({ user_id: this.user_id }, CONFIG.jwt_encryption);
    };

    Model.prototype.toWeb = function() {
        let json = this.toJSON();
        delete json.password;
        return json;
    };

    Model.prototype.getUser = function(id) {
        let json = this.findById(id, { attributes: ['id', 'first', 'last', 'email', 'phone', 'createdAt'] });
        return json;
    };

    return Model;
};
