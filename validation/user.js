const Joi = require('joi');

module.exports = {
    signup: {
        body: {
            name: Joi.string().required(),
            email_id: Joi.string().required(),
            password: Joi.string().required(),
            gender: Joi.number().required(),
            age: Joi.string().required(),
            phone_number:Joi.number().required(),
        },
    },

    signin: {
        body: {
            email_id: Joi.string().required(),
            password: Joi.string().required()
        },
    },
};
