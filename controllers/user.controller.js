const path = require('path');
const {
    User
} = require('../models');
const {
    to,
    ReE,
    ReS,
} = require('../services/util.service');
const CONFIG = require("../config/app_config");

const db = require('../models/index');
const { Op } = db.Sequelize;
const transaction = db.Sequelize.Transaction;
const sequelize = db.Sequelize;

const signup = async function(req, res) {
    let body = req.body;
    let [err, emailExist] = await to(User.findOne({
        where: { email_id: body.email_id, status: [1, 2]} //1=>active 2=>inactive
    }));
    if (err) return ReE(res, err);
    if (emailExist) return ReE(res, "Email already exist");

    let [err1, user] = await to(User.create(body));
    if (err1) return ReE(res, err1);
      let token = user.getJWT();
    user = JSON.parse(JSON.stringify(user));
    user.token = token;
    delete user.password;

    return ReS(res, "User created successfully.", user);
};
module.exports.signup = signup;

// const adminSignIn = async(req, res) => {
//     let err, admin, body = req.body;

//     [err, admin] = await to(User.findOne({
//         email_id: body.email_id,
//     }, {
//         exclude: [
//             'status',
//             'createdAt',
//             'updatedAt',
//             'deletedAt'
//         ]
//     }));
//     if (err) {
//         return success(res, false, err.message, err);
//     }

//     if (!admin) {
//         return success(res, false, "Admin does not exist!", admin);
//     }

//     if (body.password !== admin.password) {
//         return success(res, false, "Invalid password!");
//     }

//     let token = admin.getJWT();
//     admin = admin.get();
//     admin.token = token;
//     return success(res, true, "Admin Signin successful", { token });
// }
// module.exports.adminSignIn = adminSignIn;
