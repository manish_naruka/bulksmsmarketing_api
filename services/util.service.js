const { to } = require('await-to-js');
const pe = require('parse-error');


module.exports.to = async(promise) => {
    let err;
    let
        res;
    [err, res] = await to(promise);
    if (err) return [pe(err)];

    return [null, res];
};

module.exports.ReE = async function(res, err, code) { // Error Web Response
    if (typeof err === 'object' && typeof err.message != 'undefined') {
        err = err.message;
    }
    let send_data = { success: false, message: err, response: code };
    return res.json(send_data);
};

module.exports.ReS = async function(res, message, data1) { // Success Web Response
    if (typeof message != 'string' && (data1 == null || data1 == undefined)) {
        data1 = message;
        message = ""
    };
    let data;
    if (typeof code != 'undefined') res.statusCode = 200;
    let send_data = { success: true, message, response: data1 };
    return res.json(send_data);
};

module.exports.TE = TE = function(err_message, log) { // TE stands for Throw Error
    if (log === true) {
        console.error(err_message);
    }
    throw new Error(JSON.stringify(err_message));
};


