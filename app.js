const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const logger = require('morgan');
const pe = require('parse-error');
const { ReE, ReS } = require('./services/util.service');

const v1 = require('./routes/v1');

var http = require('http').Server(app);
const CONFIG = require('./config/app_config');
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use('/v1', v1);
app.use(logger('dev'));

const PORT = process.env.PORT || 3001
try {
    http.listen(PORT, function() {
        console.log('listening on *:' + PORT);
    });
} catch (error) {
    console.log(error)
}

//Log Env
console.log("Environment:", CONFIG.app)
    //DATABASE
const models = require("./models");
models.sequelize.authenticate().then(() => {
        console.log('Connected to SQL database:', CONFIG.db_name);
    })
    .catch(err => {
        console.error('Unable to connect to SQL database:', CONFIG.db_name, err);
    });




app.use('/v1', v1);
app.use('/', function(req, res) {
    res.statusCode = 200; //send the appropriate status code
    res.json({ status: "success", message: "OOP'S Somwthing Went Wrong", data: {} })
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    if (err.errors && err.errors.length > 0 && err.errors[0].messages && err.errors[0].messages.length > 0) {
        return ReE(res, err.errors[0].messages[0]);


    }
    return ReE(res, err)

    // render the error page
});

module.exports = app;

//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', error => {
    console.error('Uncaught Error', pe(error));
});
