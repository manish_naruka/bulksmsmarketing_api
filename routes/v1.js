var express = require('express');
var router = express.Router();
const validate = require('express-validation');
const userValidate = require('../validation/user');
const UserController = require('../controllers/user.controller');
// const adminSignIn = require('../controllers/user.controller');

router.post('/signup', validate(userValidate.signup), UserController.signup);
// router.post('/admin-sign-in', UserController.adminSignIn)
module.exports = router;
